# -*- coding: utf-8 -*-

import mayavi.mlab as ml
import os
import numpy as np
import scipy as sp

class Waveguide_mesh():
	"""Plots and optionally animates/saves the waveguide surfaces.
	Supported profiles are 'circular', 'ellipse_bulge' and 'star'."""
	def __init__(self):
		self.set_params(eps=0.5, h1=1.5, th=sp.linspace(0, 2 * sp.pi, 100),\
				z=sp.linspace(-10, 10, 100), profile='circular')

		def set_params(self, eps=None, h1=None, th=None, z=None,profile=None):
			if eps is not None:
				self.eps = eps
		if h1 is not None:
			self.h1 = h1
		if th is not None:
			self.th = th
		if z is not None:
			self.z = z
		if profile is not None:
			self.profile = profile

		self.tth, self.zz = sp.meshgrid(self.th, self.z)
		if hasattr(self,'rr'): del self.rr

	def get_params(self):
		print 'eps:         ' + str(self.eps)
		print 'h1:          ' + str(self.h1)
		print 'th (lnspc):  [' + str(self.th[0]) + ', ' + str(self.th[-1]) + ', ' + str(self.th.size) + ']' 
		print 'z (lnspc):   [' + str(self.z[0]) + ', ' + str(self.z[-1]) + ', ' + str(self.z.size) + ']'
		print 'profile:     ' + self.profile

	def set_profile(self):
		ze = self.zz * self.eps
		if self.profile == 'circular':
			self.rr = 1 + (self.h1 - 1) * (1 / np.cosh(ze))
		elif self.profile == 'ellipse_bulge':
			self.rr = (2 + 2*(self.h1 - 1)*sp.exp(-16*sp.sin(self.tth/2 - sp.pi/4)**2)/np.cosh(ze)) \
					/ sp.sqrt(sp.cos(self.tth)**2 + 4*sp.sin(self.tth)**2)
		elif self.profile == 'star':
			self.rr = 1 + (self.h1 - 1)*(1/np.cosh(ze))*sp.exp(-sp.sin(4*self.tth)**2/2)

	def waveguide_mesh(self,save=False):
		if not hasattr(self,'rr'):
			self.set_profile()
		xx = self.rr * sp.cos(self.tth)
		yy = self.rr * sp.sin(self.tth)

		f = ml.figure(bgcolor=(1, 1, 1), size=(800, 700))

		ml.mesh(self.zz, xx, yy, figure=f, colormap='summer')
		ml.view(azimuth=50, elevation=80, focalpoint=[1.5, 0, 0])
		ml.roll(roll=-90)
		if save == True:
			sdir = 'meshes/' + self.profile + '/'
			if not os.path.exists(sdir): os.makedirs(sdir)
			ml.savefig(self.profile + '.png', figure=f)
