# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 12:56:14 2012

@author: Simon Gaulter
"""
import scipy as sp
import scipy.special as spc
import scipy.integrate as spi
import scipy.optimize as spo
import mayavi.mlab as ml
import pylab as pl

class Circ_trapped:
	"""Provides methods to calculate the eigenvalues of the circular waveguide.
	set_params  -- method to set the required parameters;
	get_params  -- return current parameters;
	k_root      -- return wavenumbers, k_p, for one or more Airy or Airy' roots;
	k_eps       -- iterate and call 'k_root' over an array of epsilon values;
	k_h1        -- iterate and call 'k_root' over an array of h1 values;
	plot_result -- plot the data produced."""

	SOUNDSOFT = 'soundsoft'
	SOUNDHARD = 'soundhard'
	SYMMETRIC = 'symmetric'
	ANTISYMMETRIC = 'antisymmetric'

	def __init__(self):
		""" Initialise the class and set parameters.
		Defaults to finding a single k."""
		self.set_params(eps=0.1,h1=1.5,m=1,n=1,p=1,symm=self.SYMMETRIC,\
				multiple_p=False,bc=self.SOUNDSOFT)

	def get_params(self):
		"""Return the current parameters."""
		print "eps:        " + str(self.eps)
		print "h1:         " + str(self.h1)
		print "m:          " + str(self.m)
		print "n:          " + str(self.n)
		print "p:          " + str(self.p)
		print "symm:       " + str(self.symm)
		print "bc:         " + str(self.bc)
		print "multiple_p: " + str(self.multiple_p)

	def set_params(self,eps=None,h1=None,m=None,n=None,p=None,symm=None,\
			multiple_p=None,bc=None):
		"""Optionally change the parameters.

		Keyword arguments:
			eps        -- one or more values of epsilon to use;
			h1         -- one or more values of h1 to use;
			NOTE: Only one of the above may have dimension > 0 at a time
			m          -- azimuthal mode;
			n          -- radial mode;
			p          -- longitudinal mode;
			symm       -- symmetric (True) or antisymmetric (False);
			multiple_p -- use multiple (True) or a single (False) Airy root."""
		if ((isinstance(eps,sp.ndarray) and eps.size > 1) \
				and (isinstance(h1,sp.ndarray) and h1.size > 1)):
			print "Error: We can iterate over a range of EITHER h1 or epsilon."
			return
		if m is not None:
			# mth azimuthal mode
			self.m = m
		if n is not None:
			# nth radial mode
			self.n = n
		if p is not None:
			# pth longitudinal mode
			self.p = p
		if symm is not None:
			# Either symmetric or antisymmetric modes
			self.symm = symm
		if bc is not None:
			# sound hard / sound soft waveguide
			self.bc = bc
		if h1 is not None:
			# h1 max bulge height
			if isinstance(h1,sp.ndarray):
				self.h1 = h1
			else:
				self.h1 = sp.array([h1])
		if eps is not None:
			# 0 < eps << 1
			if isinstance(eps,sp.ndarray):
				self.eps = eps
			else:
				self.eps = sp.array([eps])

		# Get the mth root of the nth bessel function
		if self.bc == self.SOUNDHARD:
			self.bess = spc.jnp_zeros(self.m,self.n)[-1]
		else:
			self.bess = spc.jn_zeros(self.m,self.n)[-1]

		# Get the pth root of either Ai (antisymmetric) or Ai' (symmetric)
		if self.symm == self.SYMMETRIC:
			self.aiz = spc.ai_zeros(self.p)[1]
		else:
			self.aiz = spc.ai_zeros(self.p)[0]
		if multiple_p is not None:
			self.multiple_p = multiple_p
		if self.multiple_p is False:
			self.aiz = sp.array([self.aiz[-1]])

		# Refresh all output once parameters are changed.
		if hasattr(self,'k'): del self.k
		if hasattr(self,'k_result_h1'): del self.k_result_h1
		if hasattr(self,'k_result_eps'): del self.k_result_eps
		if hasattr(self,'phi'): del self.phi
		if hasattr(self,'lastcalled'): del self.lastcalled

	def plot_k_result(self,kind=None,title=True,show=True):
		"""kind can be 'eps' or 'h1', to specifiy result type.
		If none is specified, use the last created k_result.
		title is a boolean which determines if the plot title is printed."""

		if kind == 'h1':
			pl.plot(self.h1,self.k_result_h1)
			xl = '$h_1$'
			t = '$\epsilon = ' + str(self.eps[0]) + '$'
		elif kind == 'eps':
			pl.plot(self.eps,self.k_result_eps)
			xl = '$\epsilon$'
			t = '$h_1 = ' + str(self.h1[0]) + '$'
		else:
			if hasattr(self,'lastcalled') and self.lastcalled == 'k_eps':
				pl.plot(self.eps,self.k_result_eps)
				xl = '$\epsilon$'
				t = '$h_1 = ' + str(self.h1[0]) + '$'
			elif hasattr(self,'lastcalled') and self.lastcalled == 'k_h1':
				pl.plot(self.h1,self.k_result_h1)
				xl = '$h_1$'
				t = '$\epsilon = ' + str(self.eps[0]) + '$'
			else:
				print "No data to plot. Call method 'k_eps' or 'k_h1' first."
				return

		pl.rcParams['mathtext.default'] = 'regular'
		if title == True:
			pl.title('Wavenumbers $k$ against ' + xl + ' for $m = '\
					+ str(self.m) + '$, $n = ' + str(self.n) + '$ and ' + t + '.')
			pl.xlabel(xl)
		pl.ylabel('$k$')
		if show == True:
			pl.show()

	def k_eps(self,eps):
		"""Produces an ndarray of values, 'k_result_eps', for varying epsilon and fixed h1.
		May use one or more Airy, or Airy' roots."""
		if self.multiple_p == True:
			k_result_eps = sp.zeros([eps.size,self.p])
		else:
			k_result_eps = sp.zeros([eps.size,1])
		for i,e in enumerate(eps):
			for j,aiz in enumerate(self.aiz):
				k_result_eps[i,j] = self.k_root(eps=e)
		self.lastcalled = 'k_eps'
		return k_result_eps

	def k_h1(self,h1):
		"""Produces a matrix of values, 'k_result_h1', for varying h1 and fixed epsilon.
		May use one or more Airy, or Airy' roots."""
		if self.multiple_p == True:
			k_result_h1 = sp.zeros([h1.size,self.p])
		else:
			k_result_h1 = sp.zeros([h1.size,1])
		for i,h in enumerate(h1):
			for j,aiz in enumerate(self.aiz):
				k_result_h1[i,j] = self.k_root(h1=h)
		self.lastcalled = 'k_h1'
		return k_result_h1

	def k_root(self,eps=None,h1=None,m=None,n=None,p=None):
		"""Find a single wavenumber for one h1, epsilon and Airy or Airy' root.
		Use methods 'k_h1' or 'k_eps' to produce data for a range of h1 or eps, respectively."""
		self.set_params(m=m,n=n,p=p,eps=eps,h1=h1)
		I = lambda k: -self.g(0.,k) + self.eps**(2./3)*self.aiz
		# Find the root - Brent algorithm
		try:
			self.k = spo.brentq(I,self.bess/self.h1 \
					+ 1.E-12,self.bess - 1.E-12, maxiter=500)
		except:
			self.k = sp.nan
		return self.k

	def g(self,x,k):
		"""Return the phase function, g(x,k). Takes only positive values of x."""
		it = sp.nditer([x,None])
		for xx,y in it:
			if xx < self.xstar(k):
				y[...] = -(3./2 * spi.quad(lambda x: sp.sqrt(k**2 - (self.bess/self.h(x))**2),\
						xx, self.xstar(k))[0])**(2./3)
			else:
				y[...] = (3./2 * spi.quad(lambda x: sp.sqrt((self.bess/self.h(x))**2 - k**2),\
						self.xstar(k), xx)[0])**(2./3)
		return it.operands[1]

	def xstar(self,k):
		"""Return the turning point, x_star."""
		return sp.arccosh(k*(self.h1 - 1.)/(self.bess - k))

	def h(self,x):
		"""Return the height function, h(x)."""
		it = sp.nditer([x,None])
		for xx,y in it:
			y[...] = 1. + (self.h1 - 1.)/sp.cosh(xx)
		return it.operands[1]

	def tau(self,x):
		"""Return the transverse eigenvalue, tau_mn(x)."""
		return self.bess/self.h(x)


class Circ_mesh(Circ_trapped):
	"""Subclass to calculate phi(x,r,0) and plot the solution for the circular waveguide."""

	def __init__(self):
		"""Initialise the (sub)class. Theta will always be zero."""
		Circ_trapped.__init__(self)
		self.set_plot_params(x=[0,15,50],r=[0,1,50])
		self.th = 0

	def set_plot_params(self,x=None,r=None):
		"""Set up the linspaces for x and r. The actual region
		will be reflected across zero for each of these."""
		if x is not None:
			self.x = sp.linspace(x[0],x[1],x[2])
		if r is not None:
			self.r = sp.linspace(r[0],r[1],r[2])
		if hasattr(self,'phi'): del self.phi

	def get_params(self):
		"""Return the currently defined linspaces."""
		print "x:          [" + str(self.x[0]) + ", " + str(self.x[-1]) + ", " + str(self.x.size) + "]"
		print "r:          [" + str(self.r[0]) + ", " + str(self.r[-1]) + ", " + str(self.r.size) + "]"

		Circ_trapped.get_params(self)

	def phi_calc(self):
		"""Calculate the values of phi(x,r,0).
		This will give us a 'cross section' of the data at theta = {0,2*pi}."""
		# Calculate k for the current values if it doesn't already exist.
		self.k_root()

		# Set up the meshes.
		self.__xx,self.__rr = sp.meshgrid(self.x,self.r)
		self.__xe = self.eps*self.__xx
		self.__rr = self.__rr*self.h(self.__xe)

		# Return phi(x,r,0)
		if self.symm == True:
			phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					* sp.cos(self.m * self.th) * spc.jn(self.m,self.__rr\
					* self.tau(self.__xe)) * spc.airy( self.eps**(-2./3)\
					* self.g(self.__xe,self.k) )[0]) / (self.h(self.__xe)\
					* sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25))
		else:
			phi =(sp.absolute(self.g(self.__xe,self.k))**(.25)\
					* sp.cos(self.m * self.th) * spc.jn(self.m,self.__rr\
					* self.tau(self.__xe)) * spc.airy( self.eps**(-2./3)\
					* self.g(self.__xe,self.k) )[0]) / (self.h(self.__xe)\
					* sp.absolute(self.k**2 - self.tau(self.__xe)**2)**(.25))

		# Reflect phi around the x = 0 plane, taking symmetry
		# or antisymmetry into account.
		if self.symm == True:
			phi = sp.hstack([phi[:,-1:0:-1],phi])
		else:
			phi = sp.hstack([-phi[:,-1:0:-1],phi])

		# Reflect phi 'around' the azimuth, since cos(0) = -cos(2*pi).
		self.phi = sp.vstack([-phi[-1:0:-1],phi])
		# Reflect x and r as well, to give a suitable mesh grid.
		x = sp.hstack([-self.__xx[:,-1:0:-1],self.__xx])
		self.__x_plot = sp.vstack([x[-1:0:-1],x])
		r = sp.hstack([self.__rr[:,-1:0:-1],self.__rr])
		self.__r_plot = sp.vstack([-r[-1:0:-1],r])
		return self.phi

	def plot_phi_mesh(self,**kwargs):
		"""Plot phi(x,r,0), calculating it first if necessary."""
		if not hasattr(self,'phi'):
			self.phi_calc()
		if not hasattr(self,'fig'): self.fig = ml.figure(bgcolor=(1,1,1),size=(1600,1400))
		ml.mesh(self.__x_plot,self.__r_plot,self.phi,figure=self.fig,**kwargs)

		ml.view(azimuth=30,elevation=65,distance=30)
		ml.pitch(-5.5)

	def plot_waveguide_mesh(self,**kwargs):
		"""Plot the waveguide shell, with transparency."""
		th = sp.linspace(0, 2*sp.pi,100)

		tth,zz = sp.meshgrid(th,sp.hstack([-self.x[-1:0:-1],self.x]))
		ze = zz*self.eps

		rr = 1 + (self.h1 - 1)*(1/sp.cosh(ze))

		xx = rr*sp.cos(tth)
		yy = rr*sp.sin(tth)

		if not hasattr(self,'fig'): self.fig = ml.figure(bgcolor=(1,1,1),size=(1600,1400))
		ml.mesh(zz,xx,yy,figure=self.fig,colormap="summer",opacity=0.2,**kwargs)
		ml.view(azimuth=30,elevation=65,distance=30)
		ml.pitch(-5.5)
